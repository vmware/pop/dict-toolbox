from typing import List
from typing import Union

from dict_tools.typing import Computed
from dict_tools.typing import ComputedValue
from dict_tools.typing import is_computed


def test_computed_type():
    def _() -> Computed[str]:
        ...

    def _() -> Computed[List[str]]:
        ...

    assert Computed[str] == Union[str, ComputedValue]

    assert is_computed(Computed[str])
